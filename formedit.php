<?php
	include "koneksi.php";
	if(isset($_GET['nim'])) {
		$nim = $_GET['nim'];
	
		$sql = "select * from mahasiswa WHERE nim = '$nim'";
 		$result = mysqli_query($conn, $sql);

 		if(!$result) {
 			die("Query error : ".mysqli_errno($conn)." - ".mysqli_error($conn));
 		}

 		$data = mysqli_fetch_array($result);
 		$nim = $data['nim'];
 		$nama = $data['nama'];
 		$peminatan = $data['peminatan'];
 		$email = $data['email'];
	} else {
		header("location:tampill.php");
	}
?>

<html>
	<head>
		<title>Edit Data Mahasiswa</title>
	</head>
	<body>
		<h2>Form Isian</h2>
		<form name="input" action="prosesedit.php" method="post">
			<table>
				<tr>
					<td>NIM </td>
					<td>: <input type="text" name="nim" value="<?php echo $data['nim']; ?>" readonly></td>
				</tr>
				<tr>
					<td>Nama </td>
					<td>: <input type="text" name="nama" value="<?php echo $data['nama']; ?>" required></td>
				</tr>
				<tr>
					<td>Pemintan </td>
					<td>: 
						<select method="post" name="peminatan" style="width: 143px">
							<option>SIE</option>
							<option>SCDM</option>
						</select> 
					</td>
				</tr>
				<tr>
					<td>Email</td>
					<td>: <input type="text" name="email" value="<?php echo $data['email']; ?>" required></td>
				</tr>
			</table>
			<input type="submit" name="simpan" value="Kirim"/>
		</form>
	</body>
</html>