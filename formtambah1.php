<html>
	<title>Form Tambah mahasiswa</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
    <div id="stylized" class="myform">
        <form id="form" name="form" action="prosestambah.php" method="post">
        	<h1>-- Form Isian --</h1>
            <label>NIM
            	<span class="small">Masukan NIM</span>
        	</label>
			<input type="text" name="nim" id="name" maxlength="10" required><br>
			<label>Nama
				<span class="small">Masukan Nama</span>
			</label>
			<input type="text" name="nama" id="name" required><br>
			<label>Peminatan
				<span class="small">Pilih Peminatan</span>
			</label>
			<section class="container">
    			<div class="dropdown">
      				<select name="peminatan" class="dropdown-select">
        				<option value="SIE">Sistem Informasi Enterprise</option>
        				<option value="SCDM">Sistem Cerdas Data Mining</option>
      				</select>
    			</div>
  			</section>
			<label>Email
				<span class="small">Masukan Email</span>
			</label>
			<input type="text" name="email" id="name" required><br>
			<button type="submit">Confirm</button>
		</form>
	</div>
</html>