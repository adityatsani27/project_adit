<?php
	include "koneksi.php";
	if(isset($_GET['nim'])) {
		$nim = $_GET['nim'];
	
		$sql = "select * from mahasiswa WHERE nim = '$nim'";
 		$result = mysqli_query($conn, $sql);

 		if(!$result) {
 			die("Query error : ".mysqli_errno($conn)." - ".mysqli_error($conn));
 		}

 		$data = mysqli_fetch_array($result);
 		$nim = $data['nim'];
 		$nama = $data['nama'];
 		$peminatan = $data['peminatan'];
 		$email = $data['email'];
	} else {
		header("location:tampilll.php");
	}
?>

<html>
	<head>
		<title>Edit Data Mahasiswa</title>
	</head>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
    <div id="stylized" class="myform">
        <form id="form" name="form" action="prosesedit.php" method="post">
        	<h1>-- Form Edit Data --</h1>
            <label>NIM
            	<span class="small">Masukan NIM</span>
        	</label>
			<input type="text" name="nim" id="name" value="<?php echo $data['nim']; ?>" readonly><br>
			<label>Nama
				<span class="small">Masukan Nama</span>
			</label>
			<input type="text" name="nama" id="name" value="<?php echo $data['nama']; ?>" required><br>
			<label>Peminatan
				<span class="small">Pilih Peminatan</span>
			</label>
			<section class="container">
    			<div class="dropdown">
      				<select name="peminatan" class="dropdown-select">
        				<option value="SIE">Sistem Informasi Enterprise</option>
        				<option value="SCDM">SistemCerdas Data Mining</option>
      				</select>
    			</div>
  			</section>
			<label>Email
				<span class="small">Masukan Email</span>
			</label>
			<input type="text" name="email" id="name" value="<?php echo $data['email']; ?>" required><br>
			<button name="simpan" type="submit">Confirm</button>
		</form>
	</div>
</html>